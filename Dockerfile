FROM google/dart
MAINTAINER Matthew Gall <mgall@cloudflare.com>

WORKDIR /app
COPY . /app

EXPOSE 4040

CMD []
ENTRYPOINT ["/usr/bin/dart", "app.dart"]