import 'dart:io';

void handleHome(HttpRequest request) {
	String ip;
	final List<String> forwardedFor = request.headers['x-forwarded-for'];

	if (forwardedFor != null && forwardedFor.isNotEmpty) {
		ip = forwardedFor.join(",").split(",").first.trim();
	} else {
		ip = request.connectionInfo.remoteAddress.host;
	}

	request.response.statusCode = HttpStatus.OK;
	request.response..write(ip)..close();
}

void handleHeaders(HttpRequest request) {
	if (request.headers != null) {
		request.response.statusCode = HttpStatus.OK;
		request.response..write(request.headers)..close();
	}
}

main() async {
	var requestServer = await HttpServer.bind(InternetAddress.ANY_IP_V6, 4040);
	print('listening on ${requestServer.address.address}, port ${requestServer.port}');

	await for (HttpRequest request in requestServer) {
		if (request != null) {
			if (request.method == 'GET') {
				if (request.uri.path == "/") {
					handleHome(request);
				}
				else if (request.uri.path == "/headers") {
					handleHeaders(request);
				}
			} else {
				request.response..statusCode = HttpStatus.METHOD_NOT_ALLOWED
				..write('Unsupported request: ${request.method}.')
				..close();
			}
		}
	}
}